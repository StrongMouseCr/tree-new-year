#include <iostream>
#include <vector>
#include <Windows.h>
#include <cstdlib>
#include <ctime>

using namespace std;

void OutputTree(vector<string> tree, vector<vector<int>> emptyCell, char toy, int colorToy)
{
   HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
   for (int i = 0; i < tree.size(); i++)
   {
      for (int j = 0; j < tree[i].size(); j++)
      {
         if (i == 1 && j >= 9 && j <= 10)
         {
            SetConsoleTextAttribute(hConsole, (WORD)((6 << 4) | 7));
            cout << tree[i][j];
            SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
         }
         else if (j >= emptyCell[i][0] && j <= emptyCell[i][emptyCell[i].size() - 1] && tree[i][j] == toy)
         {
            SetConsoleTextAttribute(hConsole, (WORD)((colorToy << 4) | 7));
            cout << tree[i][j];
            SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
         }
         else
            cout << tree[i][j];
      }
      cout << endl;
   }
}

void InitialTree(vector<string>& tree, vector<vector<int>>& emptyCell, vector<vector<int>>& notEmptyCell)
{
   tree =
   {
      {"         /\\"},
      {"        <**>"},
      {"         \\/"},
      {"         /\\"},
      {"        /  \\"},
      {"       /    \\"},
      {"       /    \\"},
      {"      /      \\"},
      {"     /        \\"},
      {"     /        \\"},
      {"    /          \\"},
      {"   /            \\"},
      {"   /            \\"},
      {"  /              \\"},
      {" /________________\\"},
      {"        ||||"},
      {"     ___||||___"}
   };

   emptyCell =
   {
      {{-1}},
      {{-1}},
      {{-1}},
      {{-1}},
      {{9}, {10}},
      {{8}, {9}, {10}, {11}},
      {{8}, {9}, {10}, {11}},
      {{7}, {8}, {9}, {10}, {11}, {12}},
      {{6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}},
      {{6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}},
      {{5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}},
      {{4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}},
      {{4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}},
      {{3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}},
      {{-1}},
      {{-1}},
      {{-1}}
   };
   notEmptyCell =
   {
      {{0}},
      {{0}},
      {{0}},
      {{0}},
      {{0}, {0}},
      {{0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}},
      {{0}},
      {{0}},
      {{0}}
   };
}

void InputToyRand(vector<string>& tree, vector<vector<int>>& emptyCell, vector<vector<int>>& notEmptyCell)
{
   srand((unsigned int)time(NULL));
   for (int i = 0; i < emptyCell.size(); i++)
   {
      if (emptyCell[i][0] != -1)
      {
         for (int j = 0; j < rand() % (emptyCell[i].size() + 1) && j < 3; j++)
         {
            int cellNum = rand() % (emptyCell[i][emptyCell[i].size() - 1] - emptyCell[i][0] + 1) + emptyCell[i][0];
            tree[i][cellNum] = '0';
         }
      }
   }

   for (int i = 0; i < emptyCell.size(); i++)
   {
      for (int j = 0; j < emptyCell[i].size(); j++)
      {
         if (emptyCell[i][j] != -1)
         {
            if (tree[i][emptyCell[i][j]] == '0')
            {
               tree[i][emptyCell[i][j]] = '|';
               notEmptyCell[i][j] = 2021;
            }
         }
      }
   }
}

void ToyTwist(char newToy, vector<string>& tree, vector<vector<int>>& emptyCell, vector<vector<int>>& notEmptyCell)
{
   for (int i = 0; i < emptyCell.size(); i++)
      for (int j = 0; j < emptyCell[i].size(); j++)
         if (notEmptyCell[i][j] == 2021)
            tree[i][emptyCell[i][j]] = newToy;
}

void main()
{
   vector<string> tree;
   vector<vector<int>> emptyCell;
   vector<vector<int>> notEmptyCell;
   InitialTree(tree, emptyCell, notEmptyCell);
   InputToyRand(tree, emptyCell, notEmptyCell);
   int n = 1;
   char toy = NULL;
   int colorToy = 4;
   while (true)
   {
      switch (n)
      {
      case(0):
         toy = '|';
         n++;
         break;
      case(1):
         toy = '/';
         n++;
         break;
      case(2):
         toy = '-';
         n++;
         break;
      case(3):
         toy = '\\';
         n++;
         break;
      case(4):
         toy = '|';
         n++;
         break;
      case(5):
         toy = '/';
         n++;
         break;
      case(6):
         toy = '-';
         n++;
         break;
      case(7):
         toy = '\\';
         n = 0;
         colorToy = rand() % 3 + 2;
         break;
      }
      ToyTwist(toy, tree, emptyCell, notEmptyCell);
      OutputTree(tree, emptyCell, toy, colorToy);
      Sleep(500);
      system("cls");
   }
   system("pause");
}